import java.util.TreeMap;

public class Playlist {
			
	private static TreeMap<String, String> playlistMap  = new TreeMap<String, String>();
	
	public Playlist(){		
		playlistMap.put("favorite beats", "spotify:user:11124803756:playlist:50lqdfhDxfdlD2wLCJDIEH");	
		playlistMap.put("gas", "spotify:user:11124803756:playlist:1YIwFfjBD6juuxqg9F6VjU");	
		playlistMap.put("all songs", "spotify:user:11124803756:playlist:7h6R8xN2Vq1QuF8gogNumC");	
	}

	public String getURI(String playlist){
		return playlistMap.get(playlist);
	}
		
}
