import java.io.DataInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;

public class SpotifyMainServer {

	public static void main(String[] args) throws IOException {

		Playlist myPlaylists = new Playlist();
		
		String spotifyCommand = "";
		String msg_received = "";
		ServerSocket socket = new ServerSocket(4444);				
		
		while(!msg_received.equals("close server")){
			
			System.out.println("Waiting for message...");			
			Socket clientSocket = socket.accept();       //Blocking delay, it will wait for a message.			
			System.out.print("Message received: ");
			DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
			msg_received = dis.readUTF();	

			System.out.println(msg_received);	
			
			
			clientSocket.close();
			
			boolean notDone = true;
			
			//Play my playlist
			Reader input = new StringReader(msg_received);
			TokenScanner scanner = new TokenScanner(input);
			
			while(scanner.hasNext() && notDone){
				String token = scanner.next();
				if(token.equals("my")){
					scanner.next();
					if(scanner.next().equals("playlist")){
						System.out.println(
								msg_received.substring(msg_received.indexOf("playlist ") + "playlist ".length()));
						msg_received = "play uri " +myPlaylists.getURI(
								msg_received.substring(msg_received.indexOf("playlist ") + "playlist ".length()));
						notDone = false;
					}
				}
			}
			System.out.println(msg_received);
			spotifyCommand = "/usr/local/bin/spotify " +msg_received;
			
			Runtime.getRuntime().exec(spotifyCommand);			
		}
		
		socket.close();
		System.out.println("Server closed!");
	}
}
